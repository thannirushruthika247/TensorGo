const express = require('express');
const router = express.Router();

// Mock data
const usageDetails = {
  dataUsed: '50GB',
  services: [
    { name: 'Service A', usage: '20GB' },
    { name: 'Service B', usage: '30GB' },
  ],
};

const billingInfo = {
  currentCycle: 'July 2024',
  cumulativeUsage: '100GB',
  invoice: '$100',
};

router.get('/usage', (req, res) => {
  res.send(usageDetails);
});

router.get('/billing', (req, res) => {
  res.send(billingInfo);
});

router.get('/invoice', (req, res) => {
  // Here you would generate an invoice
  res.send({ invoice: 'Invoice generated successfully' });
});

module.exports = router;
