import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { jwtDecode } from 'jwt-decode';

const Dashboard = () => {
  const [usageDetails, setUsageDetails] = useState([]);
  const [billingDetails, setBillingDetails] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const token = localStorage.getItem('token');
      if (token) {
        // Remove this line as the decoded variable is not used
        // const decoded = jwtDecode(token);
        const config = {
          headers: { Authorization: `Bearer ${token}` },
        };

        try {
          const usageResponse = await axios.get(`${process.env.REACT_APP_API_URL}/usage`, config);
          setUsageDetails(usageResponse.data);

          const billingResponse = await axios.get(`${process.env.REACT_APP_API_URL}/billing`, config);
          setBillingDetails(billingResponse.data);
        } catch (error) {
          console.error('Error fetching data:', error);
        } finally {
          setLoading(false);
        }
      }
    };

    fetchData();
  }, []);

  const handleGenerateInvoice = async () => {
    const token = localStorage.getItem('token');
    const config = {
      headers: { Authorization: `Bearer ${token}` },
    };

    try {
      await axios.post(`${process.env.REACT_APP_API_URL}/invoice`, { userId: 'userId', usage: usageDetails }, config);
      alert('Invoice generated successfully!');
    } catch (error) {
      console.error('Error generating invoice:', error);
      alert('Failed to generate invoice.');
    }
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>Dashboard</h1>
      <h2>Usage Details</h2>
      <ul>
        {usageDetails.map((detail) => (
          <li key={detail._id}>{detail.metric}: {detail.value}</li>
        ))}
      </ul>
      <h2>Billing Details</h2>
      <p>Current Cycle: {billingDetails.currentCycle}</p>
      <p>Cumulative Usage: {billingDetails.cumulativeUsage}</p>
      <button onClick={handleGenerateInvoice}>Generate Invoice</button>
    </div>
  );
};

export default Dashboard;
