import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const Login = () => {
  const navigate = useNavigate();

  useEffect(() => {
    const handleCallbackResponse = (response) => {
      const token = response.credential;
      localStorage.setItem('token', token);
      navigate('/dashboard');
    };

    window.google.accounts.id.initialize({
      client_id: process.env.REACT_APP_GOOGLE_CLIENT_ID,
      callback: handleCallbackResponse,
    });

    window.google.accounts.id.renderButton(
      document.getElementById('signInDiv'),
      { theme: 'outline', size: 'large' }
    );
  }, [navigate]);

  return <div id="signInDiv"></div>;
};

export default Login;
