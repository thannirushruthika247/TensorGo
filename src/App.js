// App.js

import React from 'react';
import { Link } from 'react-router-dom';
import './App.css';

const App = () => {
  return (
    <div className="App"> {/* Ensure to add className="App" */}
      <h1>Welcome to SaaS Billing</h1>
      <Link to="/login">Login with Google</Link>
    </div>
  );
};

export default App;
